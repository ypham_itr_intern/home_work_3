#include<stdio.h>
#include<string.h>
#include<stdint.h>
#define Init 000
#define A 001
#define B 010
#define C 011
#define D 100
void FSM(int nums,int *state,int i)
{
     switch (*state)
     {
    case Init:
    {
        if(nums == 1)
        {
            *state = A; 
            return;
        }
        break;
    }
    case A:
    {
        if(nums == 0)
        {
            *state = B;
            return;
        }
        break;
    }
    case B:
    {
        if(nums == 1)
        {
            *state = C;
            return;
        }
        else *state = Init;
        break;
    }
    case C:
    {
        if(nums==1)
        {
            *state = D;
            printf("\nA[%d:%d]",i-3,i);
            return;
        }
        else *state = B;
        break;
    }
    case D:
    {
        if(nums==1)
        {
            *state = A;
        }
        else *state = B;
        break;
    }
    }
}
int main()
{
    int state = Init;
    int nums[] = {1,1,0,1,0,0,0,1,0,1,1,0,1,1,1,0,1,1};
    int size = sizeof(nums)/sizeof(int);
    for(int i = 0;i<size;i++)
    {
        FSM(nums[i], &state,i);
    }
    return 0;
}
